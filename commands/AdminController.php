<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\User;

class AdminController extends Controller
{

    public function actionAddUser($username)
    {
        $user = new User();
        
        $user->username = $username;
        $user->newPassword = $username;
        $user->name = $username;
        $user->birthDate = date('Y-m-d');
        
        $defaultAvatarFile = Yii::getAlias('@app') . '/resources/default-avatar.png';
        
        $user->avatar = file_get_contents($defaultAvatarFile);
        
        if ($user->save()) {
            echo "User is created";
        } else {
            var_dump($user->getErrors());
        }
    }
}