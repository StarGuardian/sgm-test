var methodSelector=Vue.component('method-selector', {
	template: '<div class="form-group"><label>API-метод:</label><select class="form-control" :value="currentMethod" @change="methodChanged">\
		<option disabled selected value="">-- Выберите метод --</option>\
		<option v-for="(method, key) in methods" :value="key">{{method.title}}</option>\
		</select></div>',
	data: function() {
		var that=this;
		
		axios.get('/?r=site/methods').then(function(response){
			that.methods=response.data;
		});
		
		return {
			methods: {},
			currentMethod: ''
		};
	},
	methods: {
		methodChanged: function (event) {
			var value=event.target.value;
			this.currentMethod=value;
			this.$emit('method-changed', this.methods[value]);
		}
	}
});

var paramForm=Vue.component('param-form', {
	template: '<div><h2 v-if="method.url">{{method.type}} {{method.url}}</h2>\
			<form enctype="multipart/form-data" :action="method.url" :method="method.type" @submit.prevent="submitRequest">\
			<div class="form-group"><label>Имя пользователя: </label><input type="text" v-model="username" class="form-control"></div>\
			<div class="form-group"><label>Пароль: </label><input type="password" v-model="password" class="form-control"></div>\
			<fieldset v-if="method.params && method.params.length>0">\
			  <legend>Параметры запроса</legend>\
			  <div class="form-group" v-for="param in method.params"><label>{{ param.title }}</label>\
			  <input type="text" class="form-control" v-model="param.value" v-if="param.type==\'text\'">\
			  <input type="file" class="form-control" @change="fileChanged($event, param)" v-if="param.type==\'file\'">\
			</div>\
			</fieldset>\
			<button type="submit" v-if="method.url" class="btn btn-primary">Отправить запрос</button>\
			</form>\
			</div>',
	props: ['method'],		
	data: function() {
		return {
			username: '',
			password: ''			
		};
	},
	methods: {
		submitRequest: function (event) {
			var that = this;
			
			var method=this.method;
			
			var url = this.generateUrl(method);
			
			var request = {
					auth: {
						username: this.username,
						password: this.password
					},
					method: method.type,
					url: url 
				}
			
			if (method.type=='POST') {
				var postParams=method.params.filter(function(param) {return !!param.value && !param.inline;});
				var data = new FormData();
				postParams.forEach(function(param) {
					if (param.type=='file') {
						data.append(param.name, param.value, param.value.name);
					} else {
						data.append(param.name, param.value);						
					}
				});
				request.data=data;
			}
			
			axios.request(request).then(function(response) {
				that.$emit('response-received', response);
			}, function (error) {
				that.$emit('response-received', error.response);
			});
		},
		generateUrl: function(method) {
			var url = method.url;
			var getParams=method.params.filter(function(param){ return !!param.value && ( method.type=='GET' || !!param.inline); });
			getParams=getParams.map(function(param) {return param.name+'='+encodeURIComponent(param.value);});
			var params=getParams.join('&');
			if (url.indexOf('?') ==-1) {
				url = url + '?';
			} else {
				url = url + '&';
			}
			return url+params;
		},
		fileChanged: function(event, param) {
			var target = event.target || event.dataTransfer;
			if (target.files.length>0) {
				param.value=target.files[0]				
			} else {
				param.value=null;
			}
		}
	}
});

var requestResult=Vue.component('request-result', {
	template: '<div class="form-group" v-if="response">\
		<h2>Результат выполнения запроса</h2>\
		<div>Код ответа: {{ response.status }} {{ response.statusText }}<div>\
		<textarea class="form-control" style="resize: vertical; height: 200px;" readonly v-model="formattedResponse">\
		</div>',
	props: ['response'],
	computed: {
		formattedResponse: function() {
			return JSON.stringify(this.response.data, null, '\t');
		}
	}
});

var app = new Vue({
  el: '#application',
  data: {
    currentMethod: {},
    currentResponse: null
  }
})