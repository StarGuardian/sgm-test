<?php

namespace app\models;

use Yii;
use app\core\Utils;
use app\core\ActiveRecord;

/**
 * This is the model class for table "{{%task}}".
 *
 * @property integer $id
 * @property string $title
 * @property integer $projectId
 * @property integer $assigneeId
 * @property string $status
 * @property string $deadline
 *
 * @property Project $project
 * @property User $assignee
 * @property TaskHistory[] $taskHistories
 */
class Task extends ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%task}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'projectId', 'assigneeId', 'status', 'deadline'], 'required'],
            [['projectId', 'assigneeId'], 'integer', 'on' => self::SCENARIO_CREATE],
            [['!projectId', '!assigneeId'], 'integer', 'on' => self::SCENARIO_UPDATE],
            [['deadline'], 'safe'],
            [['title', 'status'], 'string', 'max' => 255],
            [['projectId'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['projectId' => 'id'], 
                'filter' => ['and', ['!=', 'status', Project::STATUS_DELETED], ['ownerId' => Yii::$app->user->getId()]]],
            [['assigneeId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['assigneeId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'projectId' => Yii::t('app', 'Project ID'),
            'assigneeId' => Yii::t('app', 'Assignee ID'),
            'status' => Yii::t('app', 'Status'),
            'deadline' => Yii::t('app', 'Deadline'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'projectId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssignee()
    {
        return $this->hasOne(User::className(), ['id' => 'assigneeId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskHistories()
    {
        return $this->hasMany(TaskHistory::className(), ['taskId' => 'id']);
    }

    /**
     * @inheritdoc
     * @return TaskQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TaskQuery(get_called_class());
    }
    
    public function afterSave($insert, $changedAttributes) {        
        if ($insert) {
            $this->writeHistory(null, 'Initial assignment');
        }
        
        parent::afterSave($insert, $changedAttributes);        
    }
    
    private function writeHistory($oldAssigneeId, $comment) {
        $history=new TaskHistory();
        $history->taskId=$this->id;
        $history->oldAssigneeId=$oldAssigneeId;
        $history->newAssigneeId=$this->assigneeId;
        $history->assignDate=date('Y-m-d H:i:s');
        $history->comment=$comment;
        
        if (!$history->save()) {
            throw new \Exception(Utils::formatErrorMessage($history));
        }
        
    }
    
    public function assign($assigneeId, $comment) {        
        $tran = Yii::$app->db->beginTransaction();
        try {
            $oldAssigneeId=$this->assigneeId;            
            $this->assigneeId=$assigneeId;
            if (!$this->save()) {
                throw \Exception(Utils::formatErrorMessage($this));
            }
            $this->writeHistory($oldAssigneeId, $comment);
            $tran->commit();
        } catch (\Exception $ex) {
            $tran->rollBack();
            throw $ex;
        }
    }
}
