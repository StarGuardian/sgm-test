<?php
namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use yii\base\Security;
use yii\web\UploadedFile;
use app\core\ActiveRecord;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $name
 * @property string $birthDate
 * @property resource $avatar
 *
 * @property string $newPassword
 *
 * @property Project[] $projects
 * @property Task[] $tasks
 * @property TaskHistory[] $taskHistories
 * @property TaskHistory[] $taskHistories0
 */
class User extends ActiveRecord implements IdentityInterface {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [
                [
                    '!username',
                    '!password',
                    'name',
                    'birthDate',
                    'avatar'
                ],
                'required'
            ],
            [
                [
                    'birthDate',
                    'newPassword'
                ],
                'safe'
            ],
            // [['avatar'], 'image'],
            [
                [
                    'username',
                    'password',
                    'name'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'username'
                ],
                'unique'
            ]
        ];
    }

    public function fields() {
        $result = parent::fields();
        unset($result['avatar']);
        unset($result['password']);
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'name' => Yii::t('app', 'Name'),
            'birthDate' => Yii::t('app', 'Birth Date'),
            'avatar' => Yii::t('app', 'Avatar')
        ];
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProjects() {
        return $this->hasMany(Project::className(), [
            'ownerId' => 'id'
        ]);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTasks() {
        return $this->hasMany(Task::className(), [
            'assigneeId' => 'id'
        ]);
    }

    /**
     * @inheritdoc
     * 
     * @return UserQuery the active query used by this AR class.
     */
    public static function find() {
        return new UserQuery(get_called_class());
    }

    public static function findIdentity($id) {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        return null;
    }

    public function getId() {
        return $this->id;
    }

    public function validatePassword($password) {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public function setNewPassword($password) {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    public function getAuthKey() {
        return null;
    }

    public function validateAuthKey($authKey) {
        return false;
    }

    public function load($data, $formName = null) {
        $result = parent::load($data, $formName);
        $scope = $formName === null ? $this->formName() : $formName;
        $fieldName = ($scope === '') ? 'avatar' : "{$scope}[avatar]";
        $avatar = UploadedFile::getInstanceByName($fieldName);
        if (! empty($avatar)) {
            $this->avatar = file_get_contents($avatar->tempName);
            return true;
        }
        return $result;
    }
}
