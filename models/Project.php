<?php

namespace app\models;

use Yii;
use app\core\ActiveRecord;

/**
 * This is the model class for table "{{%project}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $creationDate
 * @property string $description
 * @property integer $ownerId
 * @property string $status
 * @property string $deadline
 *
 * @property User $owner
 * @property Task[] $tasks
 */
class Project extends ActiveRecord
{
    
    const STATUS_OK='OK';
    const STATUS_DELETED='DELETED';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ownerId'], 'default', 'value' => Yii::$app->user->getId()],
            [['creationDate'], 'default', 'value' => date('Y-m-d')],
            [['name', '!creationDate', 'description', '!ownerId', 'status', 'deadline'], 'required'],
            [['deadline'], 'safe'],
            [['description'], 'string'],
            [['ownerId'], 'integer'],
            [['name', 'status'], 'string', 'max' => 255],
            [['ownerId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['ownerId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'creationDate' => Yii::t('app', 'Creation Date'),
            'description' => Yii::t('app', 'Description'),
            'ownerId' => Yii::t('app', 'Owner ID'),
            'status' => Yii::t('app', 'Status'),
            'deadline' => Yii::t('app', 'Deadline'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'ownerId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['projectId' => 'id']);
    }

    /**
     * @inheritdoc
     * @return ProjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProjectQuery(get_called_class());
    }
    
    public function delete() {
        $this->status=self::STATUS_DELETED;
        return $this->save();
    }
}
