<?php

namespace app\models;

use Yii;
use app\core\ActiveRecord;

/**
 * This is the model class for table "{{%taskHistory}}".
 *
 * @property integer $id
 * @property integer $taskId
 * @property integer $oldAssigneeId
 * @property integer $newAssigneeId
 * @property string $assignDate
 * @property string $comment
 *
 * @property Task $task
 * @property User $oldAssignee
 * @property User $newAssignee
 */
class TaskHistory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%taskHistory}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['taskId', 'newAssigneeId', 'assignDate'], 'required'],
            [['taskId', 'oldAssigneeId', 'newAssigneeId'], 'integer'],
            [['assignDate'], 'safe'],
            [['comment'], 'string'],
            [['taskId'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['taskId' => 'id']],
            [['oldAssigneeId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['oldAssigneeId' => 'id']],
            [['newAssigneeId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['newAssigneeId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'taskId' => Yii::t('app', 'Task ID'),
            'oldAssigneeId' => Yii::t('app', 'Old Assignee ID'),
            'newAssigneeId' => Yii::t('app', 'New Assignee ID'),
            'assignDate' => Yii::t('app', 'Assign Date'),
            'comment' => Yii::t('app', 'Comment'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'taskId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOldAssignee()
    {
        return $this->hasOne(User::className(), ['id' => 'oldAssigneeId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewAssignee()
    {
        return $this->hasOne(User::className(), ['id' => 'newAssigneeId']);
    }
}
