<?php

namespace app\core;

use app\core\ActiveRecord;

class Utils {
    public static function formatErrorMessage(ActiveRecord $model) {
        $errors = $model->getErrors();
        $errors = array_map(function ($errs) {
            return implode('; ', $errs);
        }, $errors);
        return implode('; ', $errors);
    }
}
