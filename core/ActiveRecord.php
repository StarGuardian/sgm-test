<?php

namespace app\core;


class ActiveRecord extends \yii\db\ActiveRecord {
    const SCENARIO_CREATE='create';
    const SCENARIO_UPDATE='update';
    
    public function scenarios() {
        $result=parent::scenarios();
        if (!isset($result[self::SCENARIO_CREATE])) {
            $result[self::SCENARIO_CREATE]=$result[self::SCENARIO_DEFAULT];
        }
        if (!isset($result[self::SCENARIO_UPDATE])) {
            $result[self::SCENARIO_UPDATE]=$result[self::SCENARIO_DEFAULT];
        }
        return $result;
    }
}