<?php
namespace app\core;

use Yii;
use app\core\ActiveRecord;
use yii\db\ActiveQuery;

abstract class BaseController extends RestController {

    protected function verbs() {
        return [
            'index' => [
                'get'
            ],
            'create' => [
                'post'
            ],
            'update' => [
                'post'
            ],
            'delete' => [
                'post'
            ]
        ];
    }

    protected abstract function getEntityClass();

    public function actionIndex() {
        $query=$this->getIndexQuery();
        $items = $query->all();
        
        return [
            'success' => true,
            'data' => $items
        ];
    }

    /**
     * @return ActiveQuery
     */
    protected function getIndexQuery() {
        $class = $this->getEntityClass();
        return $class::find();
    }

    public function actionCreate() {
        $class = $this->getEntityClass();
        
        $model = new $class();
        $model->setScenario(ActiveRecord::SCENARIO_CREATE);
        
        return $this->fillAndSaveModel($model);
    }

    public function actionUpdate($id) {
        $class = $this->getEntityClass();
        
        $model = $class::findOne($id);
        if (empty($model)) {
            return [
                'success' => false,
                'error' => 'Not found'
            ];
        }        
        
        if (!$this->checkUpdatePermission($model)) {
            return [
                'success' => false,
                'error' => 'Forbidden'
            ];
        }
        
        $model->setScenario(ActiveRecord::SCENARIO_UPDATE);
        
        return $this->fillAndSaveModel($model);
    }

    public function actionDelete($id) {
        $class = $this->getEntityClass();
        
        $model = $class::findOne($id);
        if (empty($model)) {
            return [
                'success' => false,
                'error' => 'Not found'
            ];
        }
        
        if (!$this->checkDeletePermission($model)) {
            return [
                'success' => false,
                'error' => 'Forbidden'
            ];            
        }
        
        if ($model->delete()) {
            return [
                'success' => true
            ];
        } else {
            return [
                'success' => false,
                'error' => $this->formatErrorMessage($model)
            ];
        }
    }
    
    protected function checkUpdatePermission(ActiveRecord $model) {
        return true;
    }
    
    protected function checkDeletePermission(ActiveRecord $model) {
        return true;
    }

}