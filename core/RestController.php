<?php
namespace app\core;

use Yii;
use yii\rest\Controller;
use yii\filters\auth\HttpBasicAuth;
use app\models\User;
use app\core\ActiveRecord;

class RestController extends Controller {

    public function behaviors() {
        $behaviors = parent::behaviors();
        
        $behaviors['authenticator']['authMethods'] = [
            [
                'class' => HttpBasicAuth::className(),
                'auth' => function ($username, $password) {
                    $user = User::findOne([
                        'username' => $username
                    ]);
                    if (! empty($user) && $user->validatePassword($password)) {
                        return $user;
                    }
                    return null;
                }
            ]
        ];
        
        return $behaviors;
    }

    protected final function formatErrorMessage(ActiveRecord $model) {
        return Utils::formatErrorMessage($model);
    }

    protected function fillAndSaveModel(ActiveRecord $model) {
        $rq=Yii::$app->getRequest()->post();
        if ($model->load($rq, '')) {
            if ($model->save()) {
                return [
                    'success' => true,
                    'data' => $model
                ];
            } else {
                return [
                    'success' => false,
                    'error' => $this->formatErrorMessage($model)
                ];
            }
        } else {
            return [
                'success' => false,
                'error' => 'No data'
            ];
        }
    }
    
    protected function serializeData($content) {
        if (isset($content['data'])) {
            $data = $content['data'];
            
            if (is_array($data)) {
                $preparedData = [];
                
                foreach ($data as $item) {
                    if ($item instanceof ActiveRecord) {
                        $preparedData[] = $item->toArray();
                    } else {
                        $preparedData[] = $item;
                    }
                }
            } else {
                if ($data instanceof ActiveRecord) {
                    $preparedData = $data->toArray();
                } else {
                    $preparedData = $data;
                }
            }
            
            $content['data'] = $preparedData;
        }
        
        $result = parent::serializeData($content);
        
        return $result;
    }
}