<div id="application" class="container">
	<div class="row">
		<div class="col-xs-12">
				<method-selector @method-changed="val => currentMethod=val"></method-selector>			
		</div>
	</div>
	<param-form :method="currentMethod" @response-received="val => currentResponse=val"></param-form>
	<request-result :response="currentResponse"></request-result>
</div>