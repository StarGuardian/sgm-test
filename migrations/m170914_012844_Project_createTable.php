<?php

use yii\db\Migration;

class m170914_012844_Project_createTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%project}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()->comment('Название проекта'),
            'creationDate' => $this->date()->notNull()->comment('Дата создания проекта'),
            'description' => $this->text()->notNull()->comment('Описание проекта'),
            'ownerId' => $this->integer()->notNull()->comment('Идентификатор пользователя - владелеца проекта'),
            'status' => $this->string(255)->notNull()->comment('Статус проекта'),
            'deadline' => $this->date()->notNull()->comment('Срок выполнения проекта')
        ]);
        
        $this->createIndex('indProject_Owner', '{{%project}}', 'ownerId');
        $this->addForeignKey('fkProject_Owner', '{{%project}}', 'ownerId', '{{%user}}', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('{{%project}}');
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170914_012844_Project_createTable cannot be reverted.\n";

        return false;
    }
    */
}
