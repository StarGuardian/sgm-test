<?php

use yii\db\Migration;

class m170914_012836_User_createTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string(255)->notNull()->comment('Логин пользователя'),
            'password' => $this->string(255)->notNull()->comment('Хэш пароля пользователя'),
            'name' => $this->string(255)->notNull()->comment('ФИО пользователя'),
            'birthDate' => $this->date()->notNull()->comment('Дата рождения пользователя'),
            'avatar' => $this->binary()->notNull()->comment('Аватар пользователя')
        ]);
        
        $this->createIndex('ukUser_Username', '{{%user}}', 'username', true);
    }

    public function safeDown()
    {       
        $this->dropTable('{{%user}}');
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170914_012836_User_createTable cannot be reverted.\n";

        return false;
    }
    */
}
