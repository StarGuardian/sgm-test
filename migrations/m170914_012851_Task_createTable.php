<?php

use yii\db\Migration;

class m170914_012851_Task_createTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%task}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull()->comment('Название задачи'),
            'projectId' => $this->integer()->notNull()->comment('Идентификатор проекта, которому принадлежит задача'),
            'assigneeId' => $this->integer()->notNull()->comment('Идентификатор пользователя - исполнителя задачи'),
            'status' => $this->string(255)->notNull()->comment('Статус задачи'),
            'deadline' => $this->date()->notNull()->comment('Срок выполнения задачи')
        ]);
        
        $this->createIndex('indTask_Project', '{{%task}}', 'projectId');
        $this->createIndex('indTask_Assignee', '{{%task}}', 'assigneeId');
        $this->addForeignKey('fkTask_Project', '{{%task}}', 'projectId', '{{%project}}', 'id', 'CASCADE');
        $this->addForeignKey('fkTask_Assignee', '{{%task}}', 'assigneeId', '{{%user}}', 'id');
        
        $this->createTable('{{%taskHistory}}', [
            'id' => $this->primaryKey(),
            'taskId' => $this->integer()->notNull()->comment('Идентификатор задачи к которой относится запись в истории'),
            'oldAssigneeId' => $this->integer()->comment('Идентификатор предыдущего пользователя - исполнителя задачи'),
            'newAssigneeId' => $this->integer()->notNull()->comment('Идентификатор нового пользователя - исполнителя задачи'),
            'assignDate' => $this->date()->notNull()->comment('Дата назначения нового исполнителя'),
            'comment' => $this->text()->comment('Комментарий к назначению нового исполнителя')
        ]);
        
        $this->createIndex('indTaskHistory_Task', '{{%taskHistory}}', 'taskId');
        $this->createIndex('indTaskHistory_OldAssignee', '{{%taskHistory}}', 'oldAssigneeId');
        $this->createIndex('indTaskHistory_NewAssignee', '{{%taskHistory}}', 'newAssigneeId');
        $this->addForeignKey('fkTaskHistory_Task', '{{%taskHistory}}', 'taskId', '{{%task}}', 'id', 'CASCADE');
        $this->addForeignKey('fkTaskHistory_OldAssignee', '{{%taskHistory}}', 'oldAssigneeId', '{{%user}}', 'id');
        $this->addForeignKey('fkTaskHistory_NewAssignee', '{{%taskHistory}}', 'newAssigneeId', '{{%user}}', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('{{%taskHistory}}');
        $this->dropTable('{{%task}}');
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170914_012851_Task_createTable cannot be reverted.\n";

        return false;
    }
    */
}
