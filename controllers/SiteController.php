<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\Url;

class SiteController extends Controller {

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionMethods() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return [
            'test' => [
                'title' => 'Эхо-тест',
                'type' => 'GET',
                'url' => Url::to([
                    'site/test'
                ]),
                'params' => [
                    [
                        'title' => 'Эхо-текст',
                        'type' => 'text',
                        'name' => 'test'
                    ]
                ]
            ],
            'project_index' => [
                'title' => 'Список проектов',
                'type' => 'GET',
                'url' => Url::to([
                    'project/index'
                ]),
                'params' => []
            ],
            'project_create' => [
                'title' => 'Создание проекта',
                'type' => 'POST',
                'url' => Url::to([
                    'project/create'
                ]),
                'params' => [
                    [
                        'title' => 'Название проекта',
                        'type' => 'text',
                        'name' => 'name'
                    ],
                    [
                        'title' => 'Описание проекта',
                        'type' => 'text',
                        'name' => 'description'
                    ],
                    [
                        'title' => 'Статус',
                        'type' => 'text',
                        'name' => 'status'
                    ],
                    [
                        'title' => 'Срок проекта',
                        'type' => 'text',
                        'name' => 'deadline'
                    ]
                ]
            ],
            'project_update' => [
                'title' => 'Редактирование проекта',
                'type' => 'POST',
                'url' => Url::to([
                    'project/update'
                ]),
                'params' => [
                    [
                        'title' => 'Идентификатор проекта',
                        'type' => 'text',
                        'name' => 'id',
                        'inline' => true
                    ],
                    [
                        'title' => 'Название проекта',
                        'type' => 'text',
                        'name' => 'name'
                    ],
                    [
                        'title' => 'Описание проекта',
                        'type' => 'text',
                        'name' => 'description'
                    ],
                    [
                        'title' => 'Статус',
                        'type' => 'text',
                        'name' => 'status'
                    ],
                    [
                        'title' => 'Срок проекта',
                        'type' => 'text',
                        'name' => 'deadline'
                    ]
                ]
            ],
            'project_delete' => [
                'title' => 'Удаление проекта',
                'type' => 'POST',
                'url' => Url::to([
                    'project/delete'
                ]),
                'params' => [
                    [
                        'title' => 'Идентификатор проекта',
                        'type' => 'text',
                        'name' => 'id',
                        'inline' => true
                    ]
                ]
            ],
            'task_index' => [
                'title' => 'Список задач',
                'type' => 'GET',
                'url' => Url::to([
                    'task/index'
                ]),
                'params' => []
            ],
            'task_create' => [
                'title' => 'Создание задачи',
                'type' => 'POST',
                'url' => Url::to([
                    'task/create'
                ]),
                'params' => [
                    [
                        'title' => 'Название задачи',
                        'type' => 'text',
                        'name' => 'title'
                    ],
                    [
                        'title' => 'Идентификатор проекта',
                        'type' => 'text',
                        'name' => 'projectId'
                    ],
                    [
                        'title' => 'Идентификатор исполнителя',
                        'type' => 'text',
                        'name' => 'assigneeId'
                    ],
                    [
                        'title' => 'Статус',
                        'type' => 'text',
                        'name' => 'status'
                    ],
                    [
                        'title' => 'Срок задачи',
                        'type' => 'text',
                        'name' => 'deadline'
                    ]
                ]
            ],
            'task_update' => [
                'title' => 'Редактирование задачи',
                'type' => 'POST',
                'url' => Url::to([
                    'task/update'
                ]),
                'params' => [
                    [
                        'title' => 'Идентификатор задачи',
                        'type' => 'text',
                        'name' => 'id',
                        'inline' => true
                    ],
                    [
                        'title' => 'Название задачи',
                        'type' => 'text',
                        'name' => 'title'
                    ],
                    [
                        'title' => 'Статус',
                        'type' => 'text',
                        'name' => 'status'
                    ],
                    [
                        'title' => 'Срок задачи',
                        'type' => 'text',
                        'name' => 'deadline'
                    ]
                ]
            ],
            'task_delete' => [
                'title' => 'Удаление задачи',
                'type' => 'POST',
                'url' => Url::to([
                    'task/delete'
                ]),
                'params' => [
                    [
                        'title' => 'Идентификатор задачи',
                        'type' => 'text',
                        'name' => 'id',
                        'inline' => true
                    ]
                ]
            ],
            'task_assign' => [
                'title' => 'Переназначение задачи',
                'type' => 'POST',
                'url' => Url::to([
                    'task/assign'
                ]),
                'params' => [
                    [
                        'title' => 'Идентификатор задачи',
                        'type' => 'text',
                        'name' => 'id',
                        'inline' => true
                    ],
                    [
                        'title' => 'Идентификатор исполнителя',
                        'type' => 'text',
                        'name' => 'assigneeId'
                    ],
                    [
                        'title' => 'Комментарий',
                        'type' => 'text',
                        'name' => 'comment'
                    ]
                ]
            ],
            'user_update' => [
                'title' => 'Редактирование профиля пользователя',
                'type' => 'POST',
                'url' => Url::to([
                    'user/update'
                ]),
                'params' => [
                    [
                        'title' => 'Новый пароль',
                        'type' => 'text',
                        'name' => 'newPassword',
                    ],
                    [
                        'title' => 'Имя пользователя',
                        'type' => 'text',
                        'name' => 'name'
                    ],
                    [
                        'title' => 'День рождения',
                        'type' => 'text',
                        'name' => 'birthDate'
                    ],
                    [
                        'title' => 'Аватар',
                        'type' => 'file',
                        'name' => 'avatar'
                    ]
                ]
            ] 
        ];
    }

    public function actionTest($test = null) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'success' => true,
            'data' => $test
        ];
    }
}
