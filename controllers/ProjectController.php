<?php

namespace app\controllers;

use Yii;
use app\core\BaseController;
use app\models\Project;
use app\core\ActiveRecord;

class ProjectController extends BaseController {
    protected function getEntityClass() {
        return Project::className();
    }
    
    protected function getIndexQuery() {
        $currentUserId = Yii::$app->user->getId();
        return parent::getIndexQuery()->andWhere(['!=', 'status', Project::STATUS_DELETED])->andWhere(['ownerId' => $currentUserId]);
    }
    
    protected function checkUpdatePermission(ActiveRecord $model) {
        return $model->ownerId==Yii::$app->user->getId();
    }
    
    protected function checkDeletePermission(ActiveRecord $model) {
        return $model->ownerId==Yii::$app->user->getId();
    }
    
}