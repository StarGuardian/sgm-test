<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\User;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class AvatarController extends Controller {
    public function actionGet($id) {
        $user=User::findOne($id);
        if (empty($user)) {
            throw new NotFoundHttpException();
        }
        
        /** @var Response $response */
        $response=Yii::$app->response;
        $response->format=Response::FORMAT_RAW;
        $response->headers->add('Content-Type', 'image/jpeg');
        return stream_get_contents($user->avatar);
    }
}