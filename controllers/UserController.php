<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\core\RestController;

class UserController extends RestController {
    

    protected function verbs() {
        return [
            'update' => ['post']
        ];
    }
    
    public function actionUpdate() {
        $user=User::findOne(Yii::$app->user->getId());
        return $this->fillAndSaveModel($user);
    }
}