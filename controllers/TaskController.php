<?php
namespace app\controllers;

use Yii;
use app\core\BaseController;
use app\models\Task;
use app\models\Project;
use app\core\ActiveRecord;

class TaskController extends BaseController {

    protected function getEntityClass() {
        return Task::className();
    }

    protected function verbs() {
        $result=parent::verbs();
        $result['assign']=['post'];
        return $result;
    }
    
    protected function getIndexQuery() {
        $query = parent::getIndexQuery();
        $query->joinWith('project')->andWhere([
            '!=',
            'project.status',
            Project::STATUS_DELETED
        ])->andWhere([
            'assigneeId' => Yii::$app->user->getId()
        ]);
        return $query;
    }
    
    protected function checkUpdatePermission(ActiveRecord $model) {
        return $model->project->ownerId==Yii::$app->user->getId();
    }
    
    protected function checkDeletePermission(ActiveRecord $model) {
        return $model->project->ownerId==Yii::$app->user->getId();
    }
    
    public function actionAssign($id) {
        $model = Task::findOne($id);
        if (empty($model)) {
            return [
                'success' => false,
                'error' => 'Not found'
            ];
        }
        
        if ($model->project->ownerId!=Yii::$app->user->getId()) {
            return [
                'success' => false,
                'error' => 'Forbidden'
            ];            
        }
        
        $request=Yii::$app->request;
        $assigneeId=$request->post('assigneeId');
        $comment=$request->post('comment', '');
        try {
            $model->assign($assigneeId, $comment);
            return [
                'success' => true
            ];
        } catch (\Exception $ex) {
            return [
                'success' => false,
                'error' => $ex->getMessage()
            ];                
        }
    }
}